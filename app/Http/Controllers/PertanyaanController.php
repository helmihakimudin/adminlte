<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class PertanyaanController extends Controller
{
    //
    public function create(){
    	return view('pertanyaan.create');
    }

    public function store(Request $request){
    		// dd($request->all());

              $request->validate([
                'judul'=>'required|unique:pertanyaan',
                'isi'=>'required'

            ]);
          
    		$query = DB::table('pertanyaan')->insert([
    				"judul" => $request["judul"],
    				"isi" => $request["isi"]
    		]);



    		return redirect('/pertanyaan')->with('success', 'Data Berhasil Disimpan!');
    }


    public function index(){
        $pertanyaan = DB::table('pertanyaan')->get();
        // dd($datapertanyaan);
        return view('pertanyaan.index', compact('pertanyaan'));
    }


    public function show($id){
        $pertanyaan2 = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pertanyaan2'));
    }

    public function edit($id){
        $pertanyaan2 = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan2'));
    }

    public function update($id, Request $request){

        $request->validate([
                'judul'=>'required|unique:pertanyaan',
                'isi'=>'required'

            ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['isi']
                    ]);

        return redirect('/pertanyaan')->with('success', 'Berhasil Update!');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Berhasil delete');
    }
}
