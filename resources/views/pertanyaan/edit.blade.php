@extends('adminlte.master')

@section('content')

<div class="ml-3 mt-2 mr-3">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{$pertanyaan2->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan2->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Title</label>
                    <input type="text" class="form-control" id="judul" value="{{old('judul', $pertanyaan2->judul)}}" name="judul" placeholder="Enter Title">
                    @error('judul')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Body</label>
                    <input type="isi" class="form-control" id="isi" value="{{old('isi', $pertanyaan2->isi)}}" name="isi" placeholder="Enter Body">
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
           </div>

@endsection